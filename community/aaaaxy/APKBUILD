# Contributor: Rudolf Polzer <divVerent@gmail.com>
# Maintainer: Rudolf Polzer <divVerent@gmail.com>
pkgname=aaaaxy
pkgver=1.3.359
pkgrel=0
pkgdesc="A nonlinear puzzle platformer taking place in impossible spaces"
url="https://divVerent.github.io/aaaaxy/"
arch="all !s390x !armhf !armv7 !riscv64"
license="Apache-2.0"
makedepends="
	alsa-lib-dev
	go
	libx11-dev
	libxcursor-dev
	libxinerama-dev
	libxi-dev
	libxrandr-dev
	mesa-dev
"
checkdepends="xvfb-run mesa-dri-gallium"
source="
	https://github.com/divVerent/aaaaxy/archive/v$pkgver/aaaaxy-$pkgver.tar.gz
	https://github.com/divVerent/aaaaxy/releases/download/v$pkgver/sdl-gamecontrollerdb-for-aaaaxy-v$pkgver.zip
"
options="net"  # Needed for go mod download.

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

prepare() {
	default_prepare

	mv "$srcdir"/third_party/SDL_GameControllerDB/assets/input/* \
		third_party/SDL_GameControllerDB/assets/input/
	go mod download
}

build() {
	export AAAAXY_BUILD_USE_VERSION_FILE=true
	make BUILDTYPE=release
}

check() {
	xvfb-run sh scripts/regression-test-demo.sh aaaaxy \
		"on track for Any%, All Paths and No Teleports" \
		./aaaaxy assets/demos/benchmark.dem
}

package() {
	install -Dm644 io.github.divverent.aaaaxy.metainfo.xml \
		"$pkgdir"/usr/share/metainfo/io.github.divverent.aaaaxy.metainfo.xml
	install -Dm644 aaaaxy.png \
		"$pkgdir"/usr/share/icons/hicolor/128x128/apps/aaaaxy.png
	install -Dm644 aaaaxy.desktop \
		"$pkgdir"/usr/share/applications/aaaaxy.desktop
	install -Dm755 aaaaxy \
		"$pkgdir"/usr/bin/aaaaxy
}

sha512sums="
eb1ff24b434f3ba0fdfe5c65cb7ae34d8ca7afecd59bb0de863c49b2e811880ade3b06aaae1c8150738e98fcb1219782804aed052650046f7c870ab181af09e0  aaaaxy-1.3.359.tar.gz
6df9db12cd1f2ca26d6df6e8754710ec5b18522da4e79f36ee91f1889eac6e12d08884aa326a718803d890c6572b70687d0ea9d38b31807aa3f87484404335a7  sdl-gamecontrollerdb-for-aaaaxy-v1.3.359.zip
"
