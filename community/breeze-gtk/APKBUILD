# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=breeze-gtk
pkgver=5.27.2
pkgrel=0
pkgdesc="A GTK Theme Built to Match KDE's Breeze"
# armhf blocked by extra-cmake-modules
arch="noarch !armhf"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-only"
depends="gtk-engines"
makedepends="
	breeze
	breeze-dev
	extra-cmake-modules
	py3-cairo
	samurai
	sassc
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/breeze-gtk-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
f7df86277f23e838d35aca8f7cbc760c14916e4ac8dc0eefeb3da9e97afc921950826cea5a9969e18ab562759f15bd6653c0397b103f95d41aa951a4ebac6e22  breeze-gtk-5.27.2.tar.xz
"
