# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plymouth-kcm
pkgver=5.27.2
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/plasma/plymouth-kcm"
pkgdesc="KCM to manage the Plymouth (Boot) theme"
license="GPL-2.0-or-later"
depends="plymouth"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	plymouth-dev
	qt5-qtbase-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plymouth-kcm-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
51cf21dc2e68d6b46e6ff6e819e50a6be56149242e92145eb8c612b41ed2c8e232d78e745ee4861ffe64c5bfd4ecd36d49fab58412904781fc71912a336b0f20  plymouth-kcm-5.27.2.tar.xz
"
