# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-remotecontrollers
pkgver=5.27.2
pkgrel=0
pkgdesc="Translate various input device events into keyboard and pointer events"
url="https://invent.kde.org/plasma-bigscreen/plasma-remotecontrollers"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="BSD-2-Clause AND (GPL-2.0-only OR GPL-3.0-only) AND CC0-1.0"
# For some reason there is no dep on so:libcec but it crashes without it
depends="libcec"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kdeclarative-dev
	ki18n-dev
	knotifications-dev
	kpackage-dev
	kwindowsystem-dev
	libcec-dev
	libevdev-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtwayland-dev
	samurai
	solid-dev
	"
case "$CARCH" in
	ppc64le) ;;
	*) makedepends="$makedepends xwiimote-dev" ;;
esac

subpackages="$pkgname-lang"
case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-remotecontrollers-$pkgver.tar.xz
	uinput.conf
	"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	install -Dm644 \
		"$srcdir"/uinput.conf \
		-t "$pkgdir"/etc/modules-load.d
}

sha512sums="
95a7c0e6aeafc1d0601deb958d4b16e6914a8c72469b13384eadf97de13afc2aecd6b838c8d695d5252ec8866f8cd5347c43832264e6d0342ca14f614a98d6d5  plasma-remotecontrollers-5.27.2.tar.xz
a9b069ed121ffeee887e0583d8cb46035ecf1fa90a26a4ecb3aa11ff03178b2b08621f6676db6b2350f290694c04aabcf36f2ce3e0813a76dde9a33555edb112  uinput.conf
"
