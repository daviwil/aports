# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=layer-shell-qt
pkgver=5.27.2
pkgrel=0
pkgdesc="Qt component to allow applications to make use of the Wayland wl-layer-shell protocol"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only)"
depends_dev="
	qt5-qtdeclarative-dev
	qt5-qtwayland-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	wayland-protocols
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/layer-shell-qt-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DENABLE_TESTING=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5ce3f05b05c3666cd5e957535a4c01c5b92da51e9d2341f314a11b4c9f304cd52f36bfba7e68bfa3cd2d282d35c8a650f20b7ce99327ad68191a96752369ae25  layer-shell-qt-5.27.2.tar.xz
"
